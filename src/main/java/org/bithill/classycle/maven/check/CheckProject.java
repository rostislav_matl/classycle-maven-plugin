package org.bithill.classycle.maven.check;

import java.io.IOException;

import org.bithill.classycle.maven.Project;

import classycle.dependency.ResultRenderer;

public interface CheckProject extends Project {
  String getDependencyDefinition() throws IOException;

  boolean isFailOnUnWantedDependencies();

  Class<? extends ResultRenderer> getResultRenderer();

  String getOutputFile();
}
