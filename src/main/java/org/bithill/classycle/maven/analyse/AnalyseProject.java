package org.bithill.classycle.maven.analyse;

import org.bithill.classycle.maven.Project;

public interface AnalyseProject extends Project {

  String getTitle();

  boolean isPackagesOnly();
}
