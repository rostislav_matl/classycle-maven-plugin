package org.bithill.classycle.maven.check;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import classycle.dependency.DefaultResultRenderer;
import org.apache.commons.lang3.CharEncoding;
import org.bithill.classycle.maven.StreamFactory;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class CheckGoalTest {

  private final String                             CHECK_THAT_WILL_FAIL     = "[A] = Doesnotexit \n check sets [A]";
  private final String                             CHECK_THAT_WILL_NOT_FAIL = "[A] = * \n check sets [A]";

  @Mock
  private CheckProject                             project;

  private final Map<String, ByteArrayOutputStream> output                   = new HashMap<String, ByteArrayOutputStream>();

  private CheckGoal                                testee;

  @Before
  @SuppressWarnings("unchecked")
  public void setUp() {
    MockitoAnnotations.initMocks(this);
    when(project.getOutputDirectory()).thenReturn(testOutput());
    when(project.getExcludingClasses()).thenReturn(null);
    when(project.getReportEncoding()).thenReturn(CharEncoding.UTF_8);

    Class<DefaultResultRenderer> renderer = DefaultResultRenderer.class;
    when(project.getResultRenderer()).thenReturn((Class)renderer);

    testee = new CheckGoal(project, createStreamFactory());
  }

  private StreamFactory createStreamFactory() {
    return new StreamFactory(null) {
      @Override
      public OutputStream createStream(final String fileName)
          throws IOException {
        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        output.put(fileName, os);
        return os;
      }
    };
  }

  private String testOutput() {
    final File f = new File("target" + File.separator + "test-classes");
    return f.getAbsolutePath();
  }

  @Test
  public void shouldFailBuildWhenCheckViolatedAndFlagSet() throws IOException {
    when(project.isFailOnUnWantedDependencies()).thenReturn(true);
    when(project.getDependencyDefinition()).thenReturn(CHECK_THAT_WILL_FAIL);
    assertFalse(testee.analyse());
  }

  @Test
  public void shouldNotFailBuildWhenCheckViolatedAndFlagNotSet() throws IOException {
    when(project.isFailOnUnWantedDependencies()).thenReturn(false);
    when(project.getDependencyDefinition()).thenReturn(CHECK_THAT_WILL_FAIL);
    assertTrue(testee.analyse());
  }

  @Test
  public void shouldNotFailBuildWhenNoChecksViolatedAndFlagSet() throws IOException {
    when(project.isFailOnUnWantedDependencies()).thenReturn(true);
    when(project.getDependencyDefinition()).thenReturn(CHECK_THAT_WILL_NOT_FAIL);
    assertTrue(testee.analyse());
  }

  @Test
  public void shouldNotFailBuildWhenNotCheckViolatedAndFlagNotSet() throws IOException {
    when(project.isFailOnUnWantedDependencies()).thenReturn(false);
    when(project.getDependencyDefinition()).thenReturn(CHECK_THAT_WILL_NOT_FAIL);
    assertTrue(testee.analyse());
  }

  @Test
  public void shouldWriteReportToFile() throws IOException {
    when(project.getDependencyDefinition()).thenReturn(CHECK_THAT_WILL_NOT_FAIL);
    when(project.getOutputFile()).thenReturn(CheckMojo.TEXT_RESULT_FILE);
    testee.analyse();
    System.out.println(output.get(CheckMojo.TEXT_RESULT_FILE));
    assertThat(output.get(CheckMojo.TEXT_RESULT_FILE)).isNotNull();
  }

}
